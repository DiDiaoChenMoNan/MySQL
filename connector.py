
import mysql.connector

mydb = mysql.connector.connect(
    host="180.76.179.148",       # 数据库主机地址
    user="root",    # 数据库用户名
    passwd="yang123456@",   # 数据库密码
    database="yang_db",   # 数据库
)
mycursor = mydb.cursor()

mycursor.execute("SELECT * FROM sites")

myresult = mycursor.fetchall()     # fetchall() 获取所有记录

for x in myresult:
    print(x)

# 修改
# sql = "UPDATE sites SET name = 'ZH' WHERE name = 'RUNOOB'"

# mycursor.execute(sql)

# mydb.commit()

# print(mycursor.rowcount, " 条记录被修改")


# //删除
# sql = "DELETE FROM sites WHERE name = 'Google'"

# mycursor.execute(sql)

# mydb.commit()

# print(mycursor.rowcount, " 条记录删除")
